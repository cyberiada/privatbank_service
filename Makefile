export PATH:=virtualenv/bin:venv/bin:$(PATH)

.PHONY: help
help:
	@echo 'Setup'
	@echo '  make setup             Setup (resetup) your project'
	@echo ''
	@echo 'Clean'
	@echo '  make clean             Remove venv'
	@echo ''
	@echo 'Testing'
	@echo '  make test              Run tests'
	@echo '  make flake             Run flake8'
	@echo ''
	@echo 'Running:'
	@echo '  make run-dev           Run locally using dev server'
	@echo ''


.PHONY: setup
setup:
	sudo apt-get -y install python3-dev python3-pip libpq-dev supervisor
	make setup_venv


setup_venv: clean
	sudo pip3 install virtualenv
	virtualenv -p python3.7 venv
	pip install -r requirements/requirements.txt
	pip install -r requirements/test_requirements.txt


.PHONY: clean
	@rm -rf venv/


.PHONY: flake
flake:
	flake8 --exclude=".git,__pycache__,venv,tests,migrations" --ignore=E501
