# privatbank_service

Installation
---

    # For production environment:
    # Create virtualenv
    export VENV_DIR=venv
    python3.7 -m venv "${VENV_DIR}"
    "${VENV_DIR}"/bin/python3.7 -m ensurepip --upgrade
    "${VENV_DIR}"/bin/python3.7 -m pip install -r requirements/requirements.txt

    # For development environment:
    make setup
    
    # For development create db user and database
    CREATE ROLE privatbank_test WITH SUPERUSER CREATEDB CREATEROLE LOGIN ENCRYPTED PASSWORD 'privatbank_test';
    CREATE DATABASE privatbank_test;

Environment variables
---
These environment variables will be defined in any runtime environment and could be used in program code:

    DB_HOST              # DB listen host,      default=localhost 
    DB_PORT              # DB listen port,      default=5432
    DB_NAME              # DB Name,             default=privatbank_test 
    DB_USER              # DB user name,        default=privatbank_test
    DB_PASSWORD          # DB password,         default=privatbank_test


Run tests
---
    # Run flake8:
    make flake


Start app
---
    $ cd privatbank

    # Run migrations
    python manage.py migrate

    # Create admin
    python manage.py createsuperuser

    # Init test data to a database (set count of data what you needed)
    python manage.py init_data 1000
    
    # Purge data
    python manage.py purge_db

    # Runserver
    python manage.py runserver