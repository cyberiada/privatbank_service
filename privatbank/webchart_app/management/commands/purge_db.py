import os

from django.core.management.base import BaseCommand
from django.db import connection


class Command(BaseCommand):
    help = "Displays current time"

    def handle(self, *args, **kwargs):
        query = f"delete from webchart_app_exchange;"

        cursor = connection.cursor()
        cursor.execute(query)

        self.stdout.write(self.style.SUCCESS("Database successfully cleared"))
