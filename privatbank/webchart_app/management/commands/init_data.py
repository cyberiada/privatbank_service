import os

from django.core.management.base import BaseCommand
from django.db import connection


class Command(BaseCommand):
    help = "Displays current time"

    def add_arguments(self, parser):
        parser.add_argument("count", type=int, help="Indicates the number of users to be created")

    def handle(self, *args, **kwargs):
        count = kwargs['count']
        query = f"""\
        insert into webchart_app_exchange (value1, value2, datetime) \
        select (random()*1000)::INTEGER, \
        (random()*1000)::INTEGER, \
        timestamp '2020-01-01 00:00:00' + random() * (timestamp '2018-01-01 00:00:00' - timestamp '2018-12-31 23:59:59') \
        from generate_series(1, {count}); \
        """

        cursor = connection.cursor()
        cursor.execute(query)

        self.stdout.write(self.style.SUCCESS("Successfully insert data to database"))
