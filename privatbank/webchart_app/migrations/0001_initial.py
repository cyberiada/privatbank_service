# Generated by Django 2.2.3 on 2019-07-02 11:00

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Exchange',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('datetime', models.DateTimeField()),
                ('value1', models.IntegerField()),
                ('value2', models.IntegerField()),
            ],
        ),
    ]
