from marshmallow import Schema, fields


class BaseSchema(Schema):
    pass


class DateRange(BaseSchema):
    date_start = fields.DateTime("%d-%m-%Y %H:%M", required=True)
    date_end = fields.DateTime("%d-%m-%Y %H:%M", required=True)
