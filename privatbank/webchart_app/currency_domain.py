from .models import Exchange


def get_currency(date_start, date_end):
    filtered_currency = Exchange.objects.filter(
        datetime__gte=date_start,
        datetime__lte=date_end
    ).values()

    if not filtered_currency:
        return [], [], []

    currency_pack = (
        (currency["value1"], currency["value2"], currency["datetime"].strftime("%d-%m-%Y %H:%M"))
        for currency in filtered_currency
    )
    return zip(*currency_pack)
