from django.db import models


class Exchange(models.Model):
    datetime = models.DateTimeField()
    value1 = models.IntegerField()
    value2 = models.IntegerField()

    def __str__(self):
        return f"{self.datetime} / {self.value1} / {self.value2}"
