from django.apps import AppConfig


class WebchartAppConfig(AppConfig):
    name = 'webchart_app'
