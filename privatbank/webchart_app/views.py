import datetime

from django.http import JsonResponse
from django.shortcuts import render

from .forms import DateRange
from .currency_domain import get_currency


def index(request):
    date_start = datetime.datetime.now() - datetime.timedelta(days=1)
    date_end = datetime.datetime.now()

    context = {
        "date_start": date_start.strftime("%d-%m-%Y %H:%M"),
        "date_end": date_end.strftime("%d-%m-%Y %H:%M"),
    }

    return render(request, "webchart_app/index.html", context)


def currency(request):
    date_range = DateRange(unknown=("date_start", "date_end")).load(request.GET)
    value_one, value_two, date_time = get_currency(date_range["date_start"], date_range["date_end"])

    data = {
        "value1": value_one,
        "value2": value_two,
        "date_time": date_time
    }

    return JsonResponse(data, safe=False)
